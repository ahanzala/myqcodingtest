﻿namespace MyQ.Ceaning.Robot.Data.Models
{
    public class RobotInstructions
    {
        public string[][] Map { get; set; }
        public Point Start { get; set; }
        public string[] Commands { get; set; }
        public int Battery { get; set; }

        public RobotInstructions()
        {
            
        }
    }
}