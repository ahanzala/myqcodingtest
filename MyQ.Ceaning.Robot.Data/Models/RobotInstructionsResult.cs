﻿using System.Collections.Generic;

namespace MyQ.Ceaning.Robot.Data.Models
{
    public class RobotInstructionsResult
    {
        public List<Point> Visited { get; set; }
        public List<Point> Cleaned { get; set; }
        public Point Final { get; set; }
        public int Battery { get; set; }

        public RobotInstructionsResult()
        {
            
        }
    }
}