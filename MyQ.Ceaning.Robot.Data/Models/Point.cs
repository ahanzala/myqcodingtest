﻿namespace MyQ.Ceaning.Robot.Data.Models
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Facing { get; set; }

        public Point()
        {
            
        }
    }
}