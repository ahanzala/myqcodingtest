﻿using Autofac;
using MyQ.Cleaning.Robot.Client.Impl;
using MyQ.Cleaning.Robot.Client.Interfaces;
using MyQ.Cleaning.Robot.Services;

namespace MyQ.Cleaning.Robot.Client
{
    public class Boostrapper
    {
        private static ContainerBuilder Builder { get; set; }

        public static IContainer Container { get; private set; }

        public static void Init()
        {
            Builder = new ContainerBuilder();

            ConfigureServices(Builder);

            Container = Builder.Build();
        }

        public static void ConfigureServices(ContainerBuilder builder)
        {
            builder.RegisterModule<MyQServiceModule>();
            builder.RegisterType<CleanRobotClient>().As<ICleanRobotClient>();
        }

        public static T GetService<T>(string name = null)
        {
            return string.IsNullOrEmpty(name) ? Container.Resolve<T>() : Container.ResolveNamed<T>(name);
        }

        public static T GetServiceByKey<T>(object key)
        {
            return Container.ResolveKeyed<T>(key);
        }
    }
}
