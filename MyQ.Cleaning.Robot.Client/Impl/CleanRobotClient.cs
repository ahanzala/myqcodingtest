﻿using MyQ.Ceaning.Robot.Data.Models;
using MyQ.Cleaning.Robot.Client.Interfaces;
using MyQ.Cleaning.Robot.Services.Interfaces;
using Newtonsoft.Json;

namespace MyQ.Cleaning.Robot.Client.Impl
{
    public class CleanRobotClient : ICleanRobotClient
    {
        private readonly IFileAccessService _fileAccess;
        private readonly IRobotTraverseService _robotTraverse;

        public CleanRobotClient(IFileAccessService fileAccess,IRobotTraverseService robotTraverse)
        {
            _fileAccess = fileAccess;
            _robotTraverse = robotTraverse;
        }
        public void Run(string fileInputPath, string fileOutPath)
        {
            var fileReadResult = _fileAccess.ReadFile(fileInputPath);
            if (!string.IsNullOrEmpty(fileReadResult))
            {
                var cleanInput = JsonConvert.DeserializeObject<RobotInstructions>(fileReadResult);

                if (!_robotTraverse.IsInValidRequest(cleanInput))
                {
                    var cleanOutput = _robotTraverse.Traverse(cleanInput);
                    var fileContent = JsonConvert.SerializeObject(cleanOutput);
                    _fileAccess.WriteFile(fileOutPath, fileContent);
                }
            }
           
        }
    }
}
