﻿namespace MyQ.Cleaning.Robot.Client.Interfaces
{
    public interface ICleanRobotClient
    {
        void Run(string fileInputPath, string fileOutPath);
    }
}