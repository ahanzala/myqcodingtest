﻿using System;
using Autofac;
using MyQ.Cleaning.Robot.Client.Interfaces;

namespace MyQ.Cleaning.Robot.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Boostrapper.Init();

            if (args.Length < 2)
            {
                Console.WriteLine("Please provide fileinput and output names");
            }
            else
            {
                Boostrapper.Container.Resolve<ICleanRobotClient>().Run(args[0],args[1]);
            }
        }


    }
}
