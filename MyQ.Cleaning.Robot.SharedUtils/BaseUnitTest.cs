﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Autofac;
using Autofac.Extras.FakeItEasy;
using FakeItEasy.Sdk;
using NUnit.Framework;

namespace MyQ.Cleaning.Robot.SharedUtils
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class MockAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Field)]
    public sealed class SutAttribute : Attribute
    {
    }

    public abstract class BaseUnitTest
    {
        private readonly IList<FieldInfo> _mocks;
        private readonly IList<FieldInfo> _sut;

        protected AutoFake IoC { get; private set; }

        protected BaseUnitTest()
        {
            _mocks = new List<FieldInfo>();
            _sut = new List<FieldInfo>();

            foreach (var field in GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
            {
                if (field.GetCustomAttribute<SutAttribute>() != null)
                {
                    _sut.Add(field);
                }

                if (field.GetCustomAttribute<MockAttribute>() != null)
                {
                    _mocks.Add(field);
                }
            }
        }


        [SetUp]
        public virtual void SetUp()
        {
            var containerBuilder = new ContainerBuilder();

            SetUpMocks(containerBuilder);
            SetUpSuts(containerBuilder);
        }

        protected void SetUpMocks(ContainerBuilder builder)
        {
            foreach (var fieldInfo in _mocks)
            {
                var field = Create.Fake(fieldInfo.FieldType);
                builder.Register(c => field).As(fieldInfo.FieldType).SingleInstance();
                fieldInfo.SetValue(this, field);
            }

            IoC = new AutoFake(strict: true, builder: builder);
        }

        protected void SetUpSuts(ContainerBuilder builder)
        {
            foreach (var fieldInfo in _sut)
            {
                fieldInfo.SetValue(this, IoC.Container.Resolve(fieldInfo.FieldType));
            }
        }

        [TearDown]
        public virtual void TearDown()
        {
            IoC?.Dispose();
        }
    }
}
