﻿using System.Linq;
using AutoFixture;

namespace MyQ.Cleaning.Robot.SharedUtils
{
    /// <summary>
    /// Use this customization to prevent circular reference exception
    /// </summary>
    public class OmitOnRecursionCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Behaviors.OfType<ThrowingRecursionBehavior>()
                .ToList()
                .ForEach(b => fixture.Behaviors.Remove(b));

            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }
    }
}
