﻿using System;
using System.IO;
using System.Text;
using MyQ.Cleaning.Robot.Services.Interfaces;
using NLog;

namespace MyQ.Cleaning.Robot.Services.Impl
{
    public class FileAccessService : IFileAccessService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public FileAccessService()
        {

        }
        public string ReadFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    using (TextReader tr = new StreamReader(path))
                    {
                        return tr.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return String.Empty;
        }

        public bool WriteFile(string path, string text)
        {
            try
            {

                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                }

                using (FileStream fs = File.OpenWrite(path))
                {
                    Byte[] info = new UTF8Encoding(true)
                        .GetBytes(text);

                    fs.Write(info, 0, info.Length);
                }

                return true;

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return false;
        }
    }
}
