﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyQ.Ceaning.Robot.Data.Enum;
using MyQ.Ceaning.Robot.Data.Models;
using MyQ.Cleaning.Robot.Services.Interfaces;
using NLog;

namespace MyQ.Cleaning.Robot.Services.Impl
{
    public class RobotTraverseService : IRobotTraverseService
    {
        public string[][] Matrix { get; set; }
        public FacingEnum CurrentDirection { get; set; }
        public Point CurrentPosition { get; set; }
        public List<Point> Visited { get; set; } = new List<Point>();
        public List<Point> Cleaned { get; set; } = new List<Point>();
        public Point PreviousPosition { get ;set ; }
        protected ActionEnum CurrentAction { get; set; }
        protected int Battery { get; set; }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected string[][] RobotObstacleRecoveryCommands { get; set; } = {
            new[] { "TR", "A" },
            new[] { "TL", "B", "TR", "A" },
            new[] { "TL", "TL", "A" },
            new[] { "TR", "B", "TR", "A" },
            new[] { "TL", "TL", "A" } };

        protected string[][] DirectonActionResult { get; set; } = {
            new string[2] { "W", "E" },
            new string[2] { "E", "W" },
            new string[2] { "N", "S" },
            new string[2] { "S", "N" } };

        protected bool IsRobotOutOfObstacle { get; set; }

        public RobotTraverseService()
        {
            
        }
         
        public bool IsObstacle(string[][]matrix,Point currPosition) => CurrentPosition.X < 0 ||
                                     currPosition.Y < 0 ||
                                     currPosition.X >= matrix.GetLength(0) ||
                                     currPosition.Y >= matrix.GetLength(0) ||
                                     currPosition.X > Matrix[currPosition.Y].Length ||
                                     matrix[currPosition.Y][currPosition.X] == "C" ||
                                     matrix[currPosition.Y][currPosition.X] == "null" ||
                                     CalculateActionCost(CurrentAction) < 0;

        public bool IsInValidRequest(RobotInstructions request) => request == null || request.Battery == 0 ||
                                                                request.Commands == null || request.Commands.Length == 0 ||
                                                                request.Commands.Any(string.IsNullOrWhiteSpace) ||
                                                                !request.Commands.Any(m => (new[] { "TR", "TL", "A", "B", "C" }).Contains(m)) ||
                                                                request.Map == null || request.Start == null;

        public int CalculateActionCost(ActionEnum action)
        {
            var result = 0;
            switch (action)
            {
                case ActionEnum.TL:
                case ActionEnum.TR:
                    return Battery - 1;
                case ActionEnum.A:
                    return Battery - 2;
                case ActionEnum.B:
                    return Battery - 3;
                case ActionEnum.C:
                    return Battery - 5;
                default:
                    return result;
            }
        }

        public RobotInstructionsResult Traverse(RobotInstructions instructions)
        {
            try
            {
                var cleaningResult = new RobotInstructionsResult();
                Matrix = instructions.Map;
                CurrentDirection = (FacingEnum) Enum.Parse(typeof(FacingEnum), instructions.Start.Facing);
                Battery = instructions.Battery;
                CurrentPosition = instructions.Start;
                PreviousPosition = instructions.Start;

                var item = new Point
                {
                    X = CurrentPosition.X,
                    Y = CurrentPosition.Y
                };

                Visited.Add(item);

                RunRequestCommands(instructions.Commands);

                cleaningResult.Visited = Visited.OrderBy(m => m.X).ThenBy(m => m.Y).ToList();
                cleaningResult.Cleaned = Cleaned.OrderBy(m => m.X).ThenBy(m => m.Y).ToList();
                cleaningResult.Final = new Point() { X = CurrentPosition.X, Y = CurrentPosition.Y, Facing = CurrentDirection.ToString() };
                cleaningResult.Battery = Battery;

                return cleaningResult;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return null;
        }

        private void RunRequestCommands(string[] commands)
        {
            foreach (var command in commands)
            {
                CurrentAction = (ActionEnum)Enum.Parse(typeof(ActionEnum), command);
                RunCommand();

                if (IsObstacle(Matrix,CurrentPosition))
                {
                    CurrentPosition = PreviousPosition;
                    IsRobotOutOfObstacle = false;
                    GetRobotOutOfObstacleByRunningBackingCommands();

                    IsRobotOutOfObstacle = true;
                }
            }
        }

        private void GetRobotOutOfObstacleByRunningBackingCommands()
        {
            var backOffCommandIndex = 0;
            do
            {
                var recoveryCommands = RobotObstacleRecoveryCommands[backOffCommandIndex];
                foreach (var command in recoveryCommands)
                {
                    CurrentAction = (ActionEnum)Enum.Parse(typeof(ActionEnum), command);
                    RunCommand();
                }

                if (!IsObstacle(Matrix,CurrentPosition))
                {
                    break;
                }

                CurrentPosition = PreviousPosition;
                IsRobotOutOfObstacle = false;

                backOffCommandIndex++;
            }
            while (!IsRobotOutOfObstacle && backOffCommandIndex < RobotObstacleRecoveryCommands.GetLength(0));
        }

        private void RunCommand()
        {
            if (CalculateActionCost(CurrentAction) < 0)
            {
                return;
            }

            Battery = CalculateActionCost(CurrentAction);

            if (CurrentAction == ActionEnum.A || CurrentAction == ActionEnum.B)
            {
                PreviousPosition = CurrentPosition;
                CurrentPosition = GetResultCoordinate(CurrentDirection,
                    CurrentAction,
                    CurrentPosition);
            }
            else if (CurrentAction == ActionEnum.TL || CurrentAction == ActionEnum.TR)
            {
                CurrentDirection = (FacingEnum) Enum.Parse(typeof(FacingEnum),
                    DirectonActionResult[(int) CurrentDirection][(int) CurrentAction]);
            }

            if (!IsObstacle(Matrix,
                CurrentPosition))
            {
                if ((CurrentAction == ActionEnum.A || CurrentAction == ActionEnum.B) &&
                    (!Visited.Any(m => m.X == CurrentPosition.X && m.Y == CurrentPosition.Y)))
                {
                    Visited.Add(CurrentPosition);
                }
                else if ((CurrentAction == ActionEnum.C) &&
                         (!Cleaned.Any(m => m.X == CurrentPosition.X && m.Y == CurrentPosition.Y)))
                {
                    Cleaned.Add(CurrentPosition);
                }

            }
        }

        public Point GetResultCoordinate(FacingEnum facing, ActionEnum action, Point coordinate)
        {
            if ((facing == FacingEnum.N && action == ActionEnum.B) || (facing == FacingEnum.S && action == ActionEnum.A))
            {
                return new Point() { X = coordinate.X, Y = coordinate.Y + 1 };
            }

            if ((facing == FacingEnum.N && action == ActionEnum.A) || (facing == FacingEnum.S && action == ActionEnum.B))
            {
                return new Point { X = coordinate.X, Y = coordinate.Y - 1 };
            }

            if ((facing == FacingEnum.E && action == ActionEnum.B) || (facing == FacingEnum.W && action == ActionEnum.A))
            {
                return new Point { X = coordinate.X - 1, Y = coordinate.Y };
            }

            if ((facing == FacingEnum.E && action == ActionEnum.A) || (facing == FacingEnum.W && action == ActionEnum.B))
            {
                return new Point { X = coordinate.X + 1, Y = coordinate.Y };
            }

            return coordinate;
        }
    }
}