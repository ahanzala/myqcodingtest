﻿namespace MyQ.Cleaning.Robot.Services.Interfaces
{
    public interface IFileAccessService
    {
        string ReadFile(string path);
        bool WriteFile(string path, string text);
    }
}