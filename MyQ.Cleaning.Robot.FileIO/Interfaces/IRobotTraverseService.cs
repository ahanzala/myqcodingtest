﻿using MyQ.Ceaning.Robot.Data.Enum;
using MyQ.Ceaning.Robot.Data.Models;

namespace MyQ.Cleaning.Robot.Services.Interfaces
{
    public interface IRobotTraverseService
    {
        RobotInstructionsResult Traverse(RobotInstructions instructions);
        bool IsObstacle(string[][] matrix, Point currPosition);
        bool IsInValidRequest(RobotInstructions request);
        int CalculateActionCost(ActionEnum action);
        Point GetResultCoordinate(FacingEnum facing, ActionEnum action, Point coordinate);
    }
}