﻿using Autofac;
using MyQ.Cleaning.Robot.Services.Impl;
using MyQ.Cleaning.Robot.Services.Interfaces;

namespace MyQ.Cleaning.Robot.Services
{
    public class MyQServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(GetType().Assembly)
                .Where(t => t.IsAssignableTo<IService>())
                .AsImplementedInterfaces();

            builder.RegisterType<FileAccessService>().As<IFileAccessService>();
            builder.RegisterType<RobotTraverseService>().As<IRobotTraverseService>();

        }
    }
}
