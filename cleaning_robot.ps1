
$fileinput = $args[0]
        
 $fileoutput = $args[1]

	$csscriptPath = Split-Path $MyInvocation.MyCommand.Path
	Write-Host $csscriptPath
    $slnPath = $csscriptPath +"/MyQ.Cleaning.Robot.sln"
    $nugetPath = $csscriptPath +"/NuGet.exe"
Write-Host $slnPath
        $msBuildExe = 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe'
            Write-Host "Restoring NuGet packages" -foregroundcolor green
            &$nugetPath restore "$($slnPath)"
            Write-Host "Cleaning $($slnPath)" -foregroundcolor green
            & "$($msBuildExe)" "$($slnPath)" /t:Clean /m
       
        Write-Host "Building $($slnPath)" -foregroundcolor green
        & "$($msBuildExe)" "$($slnPath)" /t:Build /m
		
		$exePath = $csscriptPath +"/MyQ.Cleaning.Robot.Client/bin/Debug/MyQ.Cleaning.Robot.Client.exe"
		if(![System.IO.File]::Exists($exePath)){
		Write-Host "file with path"+$exePath+"doesn't exist"
		Write-Host "Please build the program first" 
		exit  1234
		}
		$psi = New-Object System.Diagnostics.processStartInfo 
		Write-Host "executing file on path:"+$exePath 
		Write-Host "With Arguments:"+$fileinput+","+$fileoutput
		& $exePath $fileinput $fileoutput

try{
Start-Process $fileoutput
}
Catch{
Write-Host "Default file opener not found, out will be written on command line"
}

   $OutputResult= Get-Content $fileoutput -ErrorAction Stop
   Write-Host $OutputResult
        