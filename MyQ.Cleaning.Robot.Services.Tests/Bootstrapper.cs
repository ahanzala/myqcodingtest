﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace MyQ.Cleaning.Robot.Services.Tests
{
    public class Bootstrapper
    {
        private static ContainerBuilder Builder { get; set; }

        public static IContainer Container { get; private set; }

        public static void Init()
        {
            Builder = new ContainerBuilder();

            ConfigureServices(Builder);

            Container = Builder.Build();
        }

        public static void ConfigureServices(ContainerBuilder builder)
        {
            builder.RegisterModule<MyQServiceModule>();
        }

        public static T GetService<T>(string name = null)
        {
            return string.IsNullOrEmpty(name) ? Container.Resolve<T>() : Container.ResolveNamed<T>(name);
        }

        public static T GetServiceByKey<T>(object key)
        {
            return Container.ResolveKeyed<T>(key);
        }
    }
}
