﻿using System;
using System.IO;
using Autofac;
using FluentAssertions;
using MyQ.Ceaning.Robot.Data.Models;
using MyQ.Cleaning.Robot.Services.Interfaces;
using MyQ.Cleaning.Robot.SharedUtils;
using Newtonsoft.Json;
using NUnit.Framework;

namespace MyQ.Cleaning.Robot.Services.Tests
{
    [TestFixture]
    public class RobotTraverseServiceUnitTests : BaseUnitTest
    {
        private IRobotTraverseService _robotTraverseService;
        private IFileAccessService _fileAccessService;
        private string InputFilepath1;
        private string OutPutFilePath1;
        private string InputFilepath2;
        private string OutPutFilePath2;

        private RobotInstructionsResult _robotInstructionsResult;
        private RobotInstructions _robotInstructions;
        [SetUp]
        public void Setup()
        {
            base.SetUp();
            Bootstrapper.Init();
            var dir = Path.GetDirectoryName(typeof(RobotTraverseServiceUnitTests).Assembly.Location);
            Environment.CurrentDirectory = dir;

           _robotTraverseService = Bootstrapper.Container.Resolve<IRobotTraverseService>();
            _fileAccessService = Bootstrapper.Container.Resolve<IFileAccessService>();

            var fileInputName = "test1.json";
            var fileOutPutName = "test1_result.json";
            var fileInputName2 = "test2.json";
            var fileOutPutName2 = "test2_result.json";

            InputFilepath1 = Path.Combine(dir, fileInputName);
             OutPutFilePath1 = Path.Combine(dir, fileOutPutName);
            InputFilepath2 = Path.Combine(dir, fileInputName2);
            OutPutFilePath2 = Path.Combine(dir, fileOutPutName2);
        }

        [Test]
        public void WhenTraverse_GivenTest1JsonSampleFile_ThenReturnResult1OutPut()
        {
            //Arrange
            var sampleInputContent = _fileAccessService.ReadFile(InputFilepath1);
            var sampleOnputContent = _fileAccessService.ReadFile(OutPutFilePath1);
            _robotInstructions = JsonConvert.DeserializeObject<RobotInstructions>(sampleInputContent);
            _robotInstructionsResult = JsonConvert.DeserializeObject<RobotInstructionsResult>(sampleOnputContent);

            //Act
            var result = _robotTraverseService.Traverse(_robotInstructions);

            //Assert
            _robotInstructionsResult.Battery.ShouldBeEquivalentTo(result.Battery);
            _robotInstructionsResult.Final.ShouldBeEquivalentTo(result.Final);
            _robotInstructionsResult.Visited.ShouldBeEquivalentTo(result.Visited);
            _robotInstructionsResult.Cleaned.ShouldBeEquivalentTo(result.Cleaned);
        }

        [Test]
        public void WhenTraverse_GivenTest2JsonSampleFile_ThenReturnResult2OutPut()
        {
            //Arrange
            var sampleInputContent = _fileAccessService.ReadFile(InputFilepath2);
            var sampleOnputContent = _fileAccessService.ReadFile(OutPutFilePath2);
            _robotInstructions = JsonConvert.DeserializeObject<RobotInstructions>(sampleInputContent);
            _robotInstructionsResult = JsonConvert.DeserializeObject<RobotInstructionsResult>(sampleOnputContent);

            //Act
            var result = _robotTraverseService.Traverse(_robotInstructions);

            //Assert
            _robotInstructionsResult.Battery.ShouldBeEquivalentTo(result.Battery);
            _robotInstructionsResult.Final.ShouldBeEquivalentTo(result.Final);
            _robotInstructionsResult.Visited.ShouldBeEquivalentTo(result.Visited);
            _robotInstructionsResult.Cleaned.ShouldBeEquivalentTo(result.Cleaned);
        }
    }
}
